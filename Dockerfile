FROM node:18-alpine

COPY package*.json ./

RUN yarn install

COPY . .

EXPOSE 1412

CMD [ "yarn", "build", "&&", "yarn", "build" ]